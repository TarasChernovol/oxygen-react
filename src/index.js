// Core styles
import './styles/reset.css';
import './styles/var.css';
import './styles/main.css';
import '../node_modules/@fortawesome/fontawesome-free/css/all.min.css';

import React from 'react';
import ReactDOM from 'react-dom';

import Todo from './containers/Todo/Todo';

ReactDOM.render(
    <React.StrictMode>
        <Todo/>
    </React.StrictMode>,
    document.getElementById('root')
);