import './styles.css';
import React from 'react';

/**
 *
 */
class _Todo extends React.Component {
    /**
     *
     * @param props
     */
    constructor(props) {
        super(props);
        this.onTriggerClickBind = this.onTriggerClick.bind(this);

    }

    /**
     *
     * @param event
     */
    onTriggerClick(event) {
        event.stopPropagation();
        event.preventDefault();
        this.setState({
            isActive: true,
        });
    }

    /**
     *
     * @returns {*}
     */
    render() {
        let x = 1;

        console.log(x);
        return <div>Test</div>
    }
}

/**
 *
 */
export default _Todo;
