import React from 'react';

import './styles.css';

const Todo = () => {
    const [todoList, setTodoList] = React.useState([]);
    const [todo, setTodo] = React.useState('');
    const [todoEdit, setTodoEdit] = React.useState(null);
    const [editText, setEditText] = React.useState(null);

    React.useEffect(() => {
        const json = localStorage.getItem('todoList');
        const loadedTodoList = JSON.parse(json);
        if (loadedTodoList) {
            setTodoList(loadedTodoList);
        }
    }, []);

    React.useEffect(() => {
        const json = JSON.stringify(todoList);
        localStorage.setItem('todoList', json);
    }, [todoList]);

    function handleSubmit(e) {
        e.preventDefault();

        const newTodo = {
            id: new Date().getTime(),
            text: todo,
            completed: false,
        };
        setTodoList([...todoList].concat(newTodo));
        setTodo('');
    }

    function deleteTodo(id) {
        let updatedTodoList = [...todoList].filter((todo) => todo.id !== id);
        setTodoList(updatedTodoList);
    }

    function toggleComplete(id) {
        let updatedTodoList = [...todoList].map((todo) => {
            if (todo.id === id) {
                todo.completed = !todo.completed;
            }
            return todo;
        });
        setTodoList(updatedTodoList);
    }

    function submitEdits(id) {
        const updatedTodoList = [...todoList].map((todo) => {
            if (todo.id === id) {
                todo.text = editText;
            }
            return todo;
        });
        setTodoList(updatedTodoList);
        setTodoEdit(null);
    }

    return (
        <div className="todo-list">

            <h1>To Do List</h1>

            <form className="todo-form" onSubmit={handleSubmit}>
                <input
                    type="text"
                    onChange={(e) => setTodo(e.target.value)}
                    value={todo}
                />
                <button type="submit"><i className="far fa-plus-square"/></button>
            </form>

            {todoList.map((todo) => (
                <div key={todo.id} className="todo">

                    <input
                        type="checkbox"
                        id="completed"
                        checked={todo.completed}
                        onChange={() => toggleComplete(todo.id)}
                    />

                    <div className="todo-text">
                        {todo.id === todoEdit ? (
                            <input
                                type="text"
                                onChange={(e) => setEditText(e.target.value)}
                            />
                        ) : (
                            <div>{todo.text}</div>
                        )}

                    </div>
                    <div className="todo-actions">

                        {todo.id === todoEdit ? (
                            <button className="icon-button" onClick={() => submitEdits(todo.id)}>
                                <i className="fas fa-save"/>
                            </button>
                        ) : (
                            <button className="icon-button" onClick={() => setTodoEdit(todo.id)}>
                                <i className="far fa-edit"/>
                            </button>
                        )}

                        <button className="icon-button" onClick={() => deleteTodo(todo.id)}>
                            <i className="far fa-trash-alt"/>
                        </button>
                    </div>
                </div>
            ))}
        </div>
    );
};

export default Todo;
